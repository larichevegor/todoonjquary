//Установка чека на 'Все задания'
$("#radioButtonAll").prop("checked", true);


// Обьявление переменных
var tasksPerPage, pageNumber;
var todos = [];
// Изменение задания двойным нажатием
function ChangeTask(elemId) {
    index = todos.findIndex(x => x.id == elemId);
    var input = $('<input />', {
        type: 'text',
        name: 'name',
        class: 'editTask',
        value: $('#' + elemId + ' #taskText').text(),
        on: {
            keydown: function (e) {
                if (e.which == 13)
                    $('#' + elemId + ' #taskText').text(this.value);
                todos[index].text = this.value;
            }
        }
    });
    $('#' + elemId + ' #taskText').html(input);

}
// Отправка формы
$("#idOfTodoForm").on('submit', function (event) {
    Show();
    Calc(todos);
});
// Нажатие на Все, Завершенные, Активные
$(".fullTaskList").on('change', '.radio', function (event) {
    ShowAfterChangeRadioButton();
    Calc(todos);
});
// Нажатие на "добавить задание"
$("#idOfPlusTask").on('click', function (event) {
    AddTask();
    Calc(todos);
});




// Добавления задания
function AddTask() {
    var taskValue = $("#idOfInputArea").val();
    if (taskValue.trim() == '') {
        alert("ВВЕДИТЕ ЗАДАНИЕ!");
    }
    else {
        var randomId = Math.floor(Math.random() * Date.now()).toString(16);
        var status = false;
        todos.push({ id: randomId, text: taskValue, completed: status });
        $("#idOfInputArea").val("");
        Calc(todos);
        Show();
    }
    return false;
}


/* Отображение блоков */
function ShowTasks(tasksList) {
    $("#idOfToDoList").empty();
    for (let i = 0; i < tasksList.length; i++) {
        var elem = $('<div/>', {
            class: 'task',
            id: tasksList[i].id,
            append: $('')
                .add("<input class='box' type='checkbox'>")
                .add('<span class="class_task_text" id="taskText">' + tasksList[i].text + '</span>')
                .add('<input class="delete" type="button" class="delete" value="X"/>'),


        });
        $(elem).appendTo('#idOfToDoList');
        $('#' + tasksList[i].id + ' .box').prop('checked', tasksList[i].completed);
    }
}

//Вызов функции смена таска по даблклику на текст

$("#idOfToDoList").on('dblclick', '.class_task_text', function (event) {
    console.log(1);
    var idOfParent = $(this).parents('.task').attr("id");
    ChangeTask(idOfParent);
});



/* Замена свойства выполнено/не выполнено */
$(".toDoList").on('change', '.box', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    /* indexOfElement = todos.findIndex(function compare(element) {
         return element.id == idOfParent;
     });*/
    indexOfElement = todos.findIndex(x => x.id == idOfParent);
    console.log(indexOfElement);
    todos[indexOfElement].completed = $(this).prop("checked");
    Calc(todos);
    if ($('input[name=filter]:checked').val() != "all") {
        ShowAfterChangeRadioButton();
    }

});

/* Удаление элемента*/
$(".toDoList").on('click', '.delete', function (event) {
    var idOfParent = $(this).parents('.task').attr("id");
    /*indexOfElement = todos.findIndex(function compare(element) {
        return element.id == idOfParent;
    });*/
    indexOfElement = todos.findIndex(x => x.id == idOfParent);
    $('#' + idOfParent).remove();
    todos.splice(indexOfElement, 1);
    Calc(todos);
    ShowAfterChangeRadioButton();
});


/* Фильтрация массива */
function Filter(valueOfRadio, tasksArray) {
    let filterValue;
    if (valueOfRadio == 'all') {
        return tasksArray;
    }
    else {
        switch (valueOfRadio) {
            case 'active':
                filterValue = false;
                break;
            case 'complete':
                filterValue = true;
                break;
        }

        return newArr = tasksArray.filter(function (el) {
            return el.completed == filterValue;
        });
    }
}
// Пагинация
function Pagination(tasksArray, valueOfRadioButton) {
    var tasksPerPage = 5; // Записей на странице
    var pageNumber = Math.ceil(tasksArray.length / tasksPerPage); // Количество страниц
    $("#idOfPages").empty();
    for (let i = 1; i <= pageNumber; i++) {
        var element = $('<li />', {
            class: 'page_number',
            id: i,
            on: {
                click: function getIdOfPage(event) {
                    $(".currentPage").removeClass("currentPage");
                    i = this.id;
                    ShowTasks(FilterMassForPage(i, Filter(valueOfRadioButton, tasksArray)));
                    Pagination(Filter(valueOfRadioButton, tasksArray), valueOfRadioButton);
                    $("#" + i).addClass("currentPage");
                }
            },
            append: $('')
                .add("<span>" + i + "</span>"),
        });
        $(element).appendTo('#idOfPages');

    }
}
// Филтрация в зависимости от страницы
function FilterMassForPage(page, tasksArray) {
    var mass;
    var lastTask = page * 5;
    var firstTask = lastTask - 5;
    return mass = tasksArray.slice(firstTask, lastTask);

}

// Подсчет заданий
function Calc(tasksArray) {
    var uncompletedCount = 0;
    var completedCount = 0;
    for (var i = 0; i < tasksArray.length; i++) {
        if (tasksArray[i].completed == false) {
            uncompletedCount += 1;
        }
        else {
            completedCount += 1;
        }
    }
    $("#spanCompleted").text('Выполнено: ' + completedCount);
    $("#spanUncompleted").text('Не выполнено: ' + uncompletedCount);

}

// Отображение отфильтрованных блоков для вставки в события
function Show() {
    var value = $('input[name=filter]:checked').val();
    var tasksPerPage = 5; // Записей на странице
    var pageNumber = Math.ceil(todos.length / tasksPerPage);
    ShowTasks(FilterMassForPage(pageNumber, Filter(value, todos)));
    Pagination(Filter(value, todos), value);

}
function ShowAfterChangeRadioButton() {
    var value = $('input[name=filter]:checked').val();
    ShowTasks(FilterMassForPage(1, Filter(value, todos)));
    Pagination(Filter(value, todos), value);
}
