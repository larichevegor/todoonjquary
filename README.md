**# README #**
## ToDo List ##
This web-app have next options:

1. Creation of new todo (by enter and by the button)
2. Change the state of each todo (done / not done) and all together
3. Deleting each todo
4. Editing todo text by double-clicking
5. Counters of the number of executed and unmapped todo
6. Display the entire list, either only executed, or only uncompleted (All Active Completed)
## Set Up ##
1. Downoload Repository
2. Lauch Todo.html
## Author ##
Larichev.E
